const fs = require('fs');
const path = require('path');

const filesFolder = './files/';
const extensions = ['.log', '.txt', '.json', 'yaml', '.xml', '.js'];


function createFile (req, res) {

  if (!req.body.filename) {
    return res.status(400).send({ message: "Please specify 'filename' parameter" });
  }

  if (!req.body.content) {
    return res.status(400).send({ message: "Please specify 'content' parameter" });
  }

  if(extensions.indexOf(path.extname(req.body.filename)) === -1) {
    return res.status(400).send({ message: "Unsupported file extension" });
  }

  fs.writeFile(path.join(__dirname, 'files', req.body.filename), req.body.content, err => {
    if(err) {    
        throw err;
    }
    res.status(200).send({ "message": "File created successfully" });
  });  
}

function getFiles(req, res) {
  fs.readdir(filesFolder, (err, files) => {
    if (err) {
      throw err;
    }
    if (files.length == 0) {
      return res.status(400).send({
        message: "Client error",
      });
    }
    res.status(200).send({
      "message": "Success",
      "files": files
    });
  });
}

const getFile = (req, res) => {

  const filePath = path.join(__dirname, 'files', req.url);

  if(!fs.existsSync(filePath)) {
    return res.status(400).send({
            'message': `No file with ${req.params.filename} filename found`,
          });
  };

  fs.readFile(filePath, (err, content) => {
    if(err) {
      throw err;
    }    
    const data = Buffer.from(content); 

  fs.stat(filePath, (err, stats) => {
      if(err) {
        throw err;
      }
      const fileDate = stats.mtime;

  res.status(200).send({
        "message": "Success",
        "filename": req.params.filename,
        "content": data.toString(),
        "extension": path.extname(filePath),
        "uploadedDate": fileDate})
      })
  });
}

module.exports = {
  createFile,
  getFiles,
  getFile
}
